<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig', [
            'participants' => $this->get('app.manager.breakfast')->getNextParticipants(),
            'day' => new \DateTime('next friday')
        ]);
    }

    /**
     * @Route("/json")
     */
    public function nextAction()
    {
        return new Response(
            $this->get('jms_serializer')->serialize($this->get('app.manager.breakfast')->getNextParticipants(), 'json'),
            Response::HTTP_OK,
            [
                'Content-type' => 'application/json'
            ]
        );
    }
}
