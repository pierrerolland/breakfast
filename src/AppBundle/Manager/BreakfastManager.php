<?php

namespace AppBundle\Manager;

use AppBundle\Repository\ParticipantRepository;

class BreakfastManager
{
    /**
     * @var ParticipantRepository
     */
    private $participantRepository;

    public function __construct(ParticipantRepository $participantRepository)
    {
        $this->participantRepository = $participantRepository;
    }

    public function getNextParticipants()
    {
        $day = new \DateTime('next friday');

        return $this->participantRepository->getNextParticipants($day);
    }
}
